package com.mxjsxz.demo.model.vo;

import java.util.List;

/**
 * @author xuwenbing
 * @date 2023-10-17
 */
public class V3TongueQuestionVO {
    /**
     * 分组标签
     */
    private String group;

    /**
     * 症状类型
     */
    private List<Symptom> types;

    /**
     * 症状类型
     */
    public static class Symptom {
        /**
         * 类型编码
         */
        private String code;

        /**
         * 类型名称
         */
        private String name;

        /**
         * 类型排序
         */
        private Integer sort;

        /**
         * 症状
         */
        private List<Option> options;

        /**
         * 各个分组的选择方式
         */
        private List<InquirySymptomGroupSelectMode> selectModes;

        /**
         * 默认选择方式
         * 0多选
         * 1单选
         */
        private Short defaultSelectMode;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getSort() {
            return sort;
        }

        public void setSort(Integer sort) {
            this.sort = sort;
        }

        public List<Option> getOptions() {
            return options;
        }

        public void setOptions(List<Option> options) {
            this.options = options;
        }

        public List<InquirySymptomGroupSelectMode> getSelectModes() {
            return selectModes;
        }

        public void setSelectModes(List<InquirySymptomGroupSelectMode> selectModes) {
            this.selectModes = selectModes;
        }

        public Short getDefaultSelectMode() {
            return defaultSelectMode;
        }

        public void setDefaultSelectMode(Short defaultSelectMode) {
            this.defaultSelectMode = defaultSelectMode;
        }
    }

    /**
     * 症状
     */
    public static class Option {
        /**
         * 症状名称
         */
        private String name;

        /**
         * 分组名称
         */
        private String groupLabel;

        /**
         * 是否选中
         */
        private Boolean checked;

        /**
         * 附件URL
         */
        private String fileUrl;

    }

    /**
     * 问诊症状分组选择方式
     *
     * @author xdl
     * @date 2022-5-26
     */
    public static class InquirySymptomGroupSelectMode {
        /**
         * 分组名称
         */
        private String groupLabel;

        /**
         * 分组选择方式
         * 0多选
         * 1单选
         */
        private Short selectMode;

    }
}
