package com.mxjsxz.demo.model.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 返回给调用者结果(中医四诊)
 *
 * @author xuwenbing
 * @date 2023-10-17
 */
public class V3TongueReturnVO implements Serializable {
    /**
     * 第三方单据id
     */
    private String outId;

    /**
     * 返回类型
     * 0:问诊题目；此时inquiry有值，result、error为空
     * 1:最终体质结果；此时result有值，error、inquiry为空
     * 2:检测失败；此时error有值，result、inquiry为空
     * 31:预判为合法图片；此时result、error、inquiry为空
     * 32:预判为不合法图片；此时error有值，result、inquiry为空
     */
    private Integer returnType;

    /**
     * 问诊问题
     */
    private List<TongueQuestionVO> inquiry;

    /**
     * 最终体质结果
     */
    private TongueResultVO result;

    /**
     * 错误提示
     */
    private TongueErrorVO error;

}
