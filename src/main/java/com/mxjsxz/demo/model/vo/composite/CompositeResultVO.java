package com.mxjsxz.demo.model.vo.composite;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mxjsxz.demo.model.vo.face.FaceResultVO;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author xuwenbing
 * @date 2022-10-18
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompositeResultVO {
    /**
     * 体质标识： 逗号隔开
     */
    private String constitutionCodes;

    /**
     * 体质名： 逗号隔开
     */
    private String constitutionNames;

    /**
     * 体质曲线排序
     */
    @NotNull
    private Integer constitutionCurveSort;

    /**
     * 体质描述
     */
    private String constitutionDescribe;

    /**
     * 证素json
     *
     * @see SyndromesElementVO
     */
    private String syndromesElementJson;

    /**
     * 证型名称（多个逗号隔开）
     */
    private String symptomName;

    /**
     * 治疗方案:json
     */
    private String treatPlanJson;

    /**
     * 体质结论配置id（多个逗号隔开）
     */
    private String resultConfigId;

    /**
     * 24节气患病率
     */
    private IllProbabilityVO illProbability;

    /**
     * 健康指数
     */
    private BigDecimal healthIndex;

    /**
     * 疾病风险评估集合
     */
    private List<DiseaseRiskVO> diseaseRisks;

    /////////////////////// 子任务结果 ///////////////////////////////
    /**
     * 子任务结果，包括舌象、面部等
     * <pre>
     * {
     *   "tongue": {...},
     *   "face": {...}
     * }
     * </pre>
     *
     * @see CompositeTongueResultVO
     * @see FaceResultVO
     */
    private Map<String, Object> characterMap;

    public String getConstitutionCodes() {
        return constitutionCodes;
    }

    public void setConstitutionCodes(String constitutionCodes) {
        this.constitutionCodes = constitutionCodes;
    }

    public String getConstitutionNames() {
        return constitutionNames;
    }

    public void setConstitutionNames(String constitutionNames) {
        this.constitutionNames = constitutionNames;
    }

    public Integer getConstitutionCurveSort() {
        return constitutionCurveSort;
    }

    public void setConstitutionCurveSort(Integer constitutionCurveSort) {
        this.constitutionCurveSort = constitutionCurveSort;
    }

    public String getConstitutionDescribe() {
        return constitutionDescribe;
    }

    public void setConstitutionDescribe(String constitutionDescribe) {
        this.constitutionDescribe = constitutionDescribe;
    }

    public String getSyndromesElementJson() {
        return syndromesElementJson;
    }

    public void setSyndromesElementJson(String syndromesElementJson) {
        this.syndromesElementJson = syndromesElementJson;
    }

    public String getSymptomName() {
        return symptomName;
    }

    public void setSymptomName(String symptomName) {
        this.symptomName = symptomName;
    }

    public String getTreatPlanJson() {
        return treatPlanJson;
    }

    public void setTreatPlanJson(String treatPlanJson) {
        this.treatPlanJson = treatPlanJson;
    }

    public String getResultConfigId() {
        return resultConfigId;
    }

    public void setResultConfigId(String resultConfigId) {
        this.resultConfigId = resultConfigId;
    }

    public IllProbabilityVO getIllProbability() {
        return illProbability;
    }

    public void setIllProbability(IllProbabilityVO illProbability) {
        this.illProbability = illProbability;
    }

    public BigDecimal getHealthIndex() {
        return healthIndex;
    }

    public void setHealthIndex(BigDecimal healthIndex) {
        this.healthIndex = healthIndex;
    }

    public List<DiseaseRiskVO> getDiseaseRisks() {
        return diseaseRisks;
    }

    public void setDiseaseRisks(List<DiseaseRiskVO> diseaseRisks) {
        this.diseaseRisks = diseaseRisks;
    }

    public Map<String, Object> getCharacterMap() {
        return characterMap;
    }

    public void setCharacterMap(Map<String, Object> characterMap) {
        this.characterMap = characterMap;
    }
}
