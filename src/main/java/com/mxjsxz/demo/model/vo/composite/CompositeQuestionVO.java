package com.mxjsxz.demo.model.vo.composite;

import java.util.List;

/**
 * 返回的问诊问题
 *
 * @author xuwenbing
 * @date 2020-07-28
 */

public class CompositeQuestionVO {

    /**
     * 问题列表
     */
    List<InquiryQuestion> questionList;

    /**
     * 问题分组列表
     */
    List<InquiryQuestionGroup> questionGroupList;

    public static class InquiryQuestion {
        /**
         * 问题序号
         */
        private Integer questionIndex;

        /**
         * 问题对应答案 可选项,多个选项间 逗号隔开
         */
        private String answerOptions;

        /**
         * 问题对应答案 子项,多个选项间 逗号隔开
         */
        private String subAnswerOptions;

        /**
         * 2022-05-12 增加答案说明
         * 问题答案说明Json ['', '']
         */
        private String answerOptionsDescJson;

        /**
         * 问题默认选项
         */
        private String defaultOption;

        /**
         * 2020--01-13 问题增加主要表现
         */
        private String mainPerformance;

        // ---------- V2版本字段（以上字段可单独使用） ---------
        /**
         * 问题内容
         */
        private String questionContent;
        /**
         * <p>问题答案多媒体Json</p>
         * <p>1、格式如下：</p>
         * <pre>
         *  [{
         * “title”:”标题”,
         * “combinationType”:”组合方式”,
         * “audio ”:”音频地址”,
         * “video”:”视频地址”,
         * “image”:”图片地址”,
         * “animation”:”动画”
         * }]
         * </pre>
         * <p>2、combinationType：</p>
         * <pre>
         *  0：无组合
         *  1：图片(有title)+音频
         *  2：动画(json文件)(有title)+音频
         *  3：视频(有title)
         * </pre>
         * <p>3、顺序对应于answerOptions</p>
         */
        private List<MultiMedia> answerOptionsMultiMediaList;

        /**
         * <p>问题多媒体Json</p>
         * <p>1、格式如下：</p>
         * <pre>
         *  {
         * “title”:”标题”,
         * “combinationType”:”组合方式”,
         * “audio ”:”音频地址”,
         * “video”:”视频地址”,
         * “image”:”图片地址”,
         * “animation”:”动画”
         * }
         * </pre>
         * <p>2、combinationType：</p>
         * <pre>
         *  0：无组合
         *  1：图片(有title)+音频
         *  2：动画(json文件)(有title)+音频
         *  3：视频(有title)
         * </pre>
         */
        private MultiMedia questionContentMultiMedia;

        public Integer getQuestionIndex() {
            return questionIndex;
        }

        public void setQuestionIndex(Integer questionIndex) {
            this.questionIndex = questionIndex;
        }

        public String getAnswerOptions() {
            return answerOptions;
        }

        public void setAnswerOptions(String answerOptions) {
            this.answerOptions = answerOptions;
        }

        public String getSubAnswerOptions() {
            return subAnswerOptions;
        }

        public void setSubAnswerOptions(String subAnswerOptions) {
            this.subAnswerOptions = subAnswerOptions;
        }

        public String getAnswerOptionsDescJson() {
            return answerOptionsDescJson;
        }

        public void setAnswerOptionsDescJson(String answerOptionsDescJson) {
            this.answerOptionsDescJson = answerOptionsDescJson;
        }

        public String getDefaultOption() {
            return defaultOption;
        }

        public void setDefaultOption(String defaultOption) {
            this.defaultOption = defaultOption;
        }

        public String getMainPerformance() {
            return mainPerformance;
        }

        public void setMainPerformance(String mainPerformance) {
            this.mainPerformance = mainPerformance;
        }

        public String getQuestionContent() {
            return questionContent;
        }

        public void setQuestionContent(String questionContent) {
            this.questionContent = questionContent;
        }

        public List<MultiMedia> getAnswerOptionsMultiMediaList() {
            return answerOptionsMultiMediaList;
        }

        public void setAnswerOptionsMultiMediaList(List<MultiMedia> answerOptionsMultiMediaList) {
            this.answerOptionsMultiMediaList = answerOptionsMultiMediaList;
        }

        public MultiMedia getQuestionContentMultiMedia() {
            return questionContentMultiMedia;
        }

        public void setQuestionContentMultiMedia(MultiMedia questionContentMultiMedia) {
            this.questionContentMultiMedia = questionContentMultiMedia;
        }
    }

    public static class InquiryQuestionGroup {
        /**
         * 分组排序
         */
        private Integer groupIndex;

        /**
         * 分组内容
         */
        private String groupContent;

        /**
         * <p>分组内容多媒体Json</p>
         * <p>1、格式如下：</p>
         * <pre>
         *  {
         * “title”:”标题”,
         * “combinationType”:”组合方式”,
         * “audio ”:”音频地址”,
         * “video”:”视频地址”,
         * “image”:”图片地址”,
         * “animation”:”动画”
         * }
         * </pre>
         * <p>2、combinationType：</p>
         * <pre>
         *  0：无组合
         *  1：图片(有title)+音频
         *  2：动画(json文件)(有title)+音频
         *  3：视频(有title)
         * </pre>
         */
        private MultiMedia groupContentMultiMedia;

        /**
         * 问诊问题配置表Index列表
         */
        private List<String> questionIndexList;

        public Integer getGroupIndex() {
            return groupIndex;
        }

        public void setGroupIndex(Integer groupIndex) {
            this.groupIndex = groupIndex;
        }

        public String getGroupContent() {
            return groupContent;
        }

        public void setGroupContent(String groupContent) {
            this.groupContent = groupContent;
        }

        public MultiMedia getGroupContentMultiMedia() {
            return groupContentMultiMedia;
        }

        public void setGroupContentMultiMedia(MultiMedia groupContentMultiMedia) {
            this.groupContentMultiMedia = groupContentMultiMedia;
        }

        public List<String> getQuestionIndexList() {
            return questionIndexList;
        }

        public void setQuestionIndexList(List<String> questionIndexList) {
            this.questionIndexList = questionIndexList;
        }
    }

    public static class MultiMedia {
        /**
         * 标题
         */
        private String title;

        /**
         * 组合方式
         */
        private Integer combinationType;

        /**
         * 音频地址
         */
        private String audio;

        /**
         * 视频地址
         */
        private String video;

        /**
         * 图片地址
         */
        private String image;

        /**
         * 动画
         */
        private String animation;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getCombinationType() {
            return combinationType;
        }

        public void setCombinationType(Integer combinationType) {
            this.combinationType = combinationType;
        }

        public String getAudio() {
            return audio;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getAnimation() {
            return animation;
        }

        public void setAnimation(String animation) {
            this.animation = animation;
        }
    }

    public List<InquiryQuestion> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<InquiryQuestion> questionList) {
        this.questionList = questionList;
    }

    public List<InquiryQuestionGroup> getQuestionGroupList() {
        return questionGroupList;
    }

    public void setQuestionGroupList(List<InquiryQuestionGroup> questionGroupList) {
        this.questionGroupList = questionGroupList;
    }
}
