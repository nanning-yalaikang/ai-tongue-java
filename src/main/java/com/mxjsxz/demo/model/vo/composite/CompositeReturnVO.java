package com.mxjsxz.demo.model.vo.composite;

import com.mxjsxz.demo.model.vo.TongueErrorVO;

import java.util.Map;

/**
 * 综合任务 return vo
 *
 * @author xuwenbing
 * @date 2022-10-18
 */
public class CompositeReturnVO {
    /**
     * 第三方单据id
     */
    private String outId;

    /**
     * <strong>返回类型:</strong>
     * <p>-2:AI未处理</p>
     * <p>-1:AI处理中</p>
     * <p>0:问诊题目</p>
     * <p>1:最终体质结果</p>
     * <p>2:检测失败</p>
     * <p>31:预判为合法图片</p>
     * <p>32:预判为不合法图片</p>
     */
    private Integer returnType;

    /**
     * 问诊轮次
     * returnType=0 时使用
     */
    private Integer inquiryRound;

    /**
     * 问题列表，具有分组和多媒体等信息
     */
    private CompositeQuestionVO question;

    /**
     * 错误提示
     */
    private Map<String, TongueErrorVO> errorMap;

    /**
     * 结果
     */
    private CompositeResultVO result;

    /**
     * 国际化资源
     */
    private LangVO lang;

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public Integer getReturnType() {
        return returnType;
    }

    public void setReturnType(Integer returnType) {
        this.returnType = returnType;
    }

    public Integer getInquiryRound() {
        return inquiryRound;
    }

    public void setInquiryRound(Integer inquiryRound) {
        this.inquiryRound = inquiryRound;
    }

    public CompositeQuestionVO getQuestion() {
        return question;
    }

    public void setQuestion(CompositeQuestionVO question) {
        this.question = question;
    }

    public Map<String, TongueErrorVO> getErrorMap() {
        return errorMap;
    }

    public void setErrorMap(Map<String, TongueErrorVO> errorMap) {
        this.errorMap = errorMap;
    }

    public CompositeResultVO getResult() {
        return result;
    }

    public void setResult(CompositeResultVO result) {
        this.result = result;
    }

    public LangVO getLang() {
        return lang;
    }

    public void setLang(LangVO lang) {
        this.lang = lang;
    }
}
