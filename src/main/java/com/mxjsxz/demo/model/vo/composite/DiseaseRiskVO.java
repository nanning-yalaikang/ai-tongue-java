package com.mxjsxz.demo.model.vo.composite;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

/**
 * 疾病风险评估
 *
 * @author xdl
 * @date 2023-10-21
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DiseaseRiskVO {

    /**
     * 疾病名称
     */
    private String diseaseName;

    /**
     * 风险指数
     */
    private BigDecimal riskIndex;

    /**
     * 风险值
     */
    private String riskName;

    /**
     * 风险等级
     */
    private String riskLevel;

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public BigDecimal getRiskIndex() {
        return riskIndex;
    }

    public void setRiskIndex(BigDecimal riskIndex) {
        this.riskIndex = riskIndex;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public String getRiskLevel() {
        return riskLevel;
    }

    public void setRiskLevel(String riskLevel) {
        this.riskLevel = riskLevel;
    }
}
