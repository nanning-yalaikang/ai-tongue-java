package com.mxjsxz.demo.model.vo.composite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mxjsxz.demo.model.vo.TongueResultVO;

import java.util.List;

/**
 * @author xuwenbing
 * @date 2022-10-18
 */
public class CompositeTongueResultVO {
    /**
     * 舌面原图（矫正后）
     */
    private String ossOriImgUrl;

    /**
     * 舌下原图
     */
    private String ossOriBackImgUrl;

    /**
     * 舌象属性
     */
    private List<TongueResultVO.TongueAttr> tongueAttrList;

    /**
     * 过程图
     */
    private ProcessImage processImage;

    /**
     * 计算图
     */
    private CaluateImage caluateImage;

    /**
     * 舌色
     */
    private TongueCharacterVO colorOfTongue;

    /**
     * 苔色
     */
    private TongueCharacterVO colorOfMoss;

    /**
     * 舌形
     */
    private TongueCharacterVO shapeOfTongue;

    /**
     * 苔质
     */
    private TongueCharacterVO moss;

    /**
     * 津液
     */
    private TongueCharacterVO bodyfluid;

    /**
     * 络脉
     */
    private TongueCharacterVO vein;

    /**
     * 过程图
     */
    public static class ProcessImage {
        /**
         * 裂纹,形如：https://xxxxxx/ai/liewen/2022/09/6bd86ddb-6ac8-44b5-97fb-bb09a14f6f9d.png
         */
        private String liewen;

        /**
         * 剥落
         */
        private String boluo;

        /**
         * 点刺
         */
        private String prick;

        /**
         * 瘀点
         */
        private String yudian;

        /**
         * 舌色
         */
        private String shese;

        /**
         * 苔色
         */
        private String taise;

        /**
         * 舌形
         */
        private String shexing;

        /**
         * 络脉
         */
        private String luomaiLabel;

        /**
         * 齿痕
         */
        private String toothtrace;

        /**
         * 瘀斑
         */
        private String yuban;

        /**
         * 舌面分割分区图
         */
        private String splitAreaLabel;

        /**
         * HSV分图H
         */
        private String hsvH;

        /**
         * HSV分图S
         */
        private String hsvS;

        /**
         * HSV分图V
         */
        private String hsvV;

        // 20230420新增
        /**
         * 异常舌形
         */
        private String rectContour;

        /**
         * 肝郁线
         */
        private String ganyuXian;

        public String getLiewen() {
            return liewen;
        }

        public void setLiewen(String liewen) {
            this.liewen = liewen;
        }

        public String getBoluo() {
            return boluo;
        }

        public void setBoluo(String boluo) {
            this.boluo = boluo;
        }

        public String getPrick() {
            return prick;
        }

        public void setPrick(String prick) {
            this.prick = prick;
        }

        public String getYudian() {
            return yudian;
        }

        public void setYudian(String yudian) {
            this.yudian = yudian;
        }

        public String getShese() {
            return shese;
        }

        public void setShese(String shese) {
            this.shese = shese;
        }

        public String getTaise() {
            return taise;
        }

        public void setTaise(String taise) {
            this.taise = taise;
        }

        public String getShexing() {
            return shexing;
        }

        public void setShexing(String shexing) {
            this.shexing = shexing;
        }

        public String getLuomaiLabel() {
            return luomaiLabel;
        }

        public void setLuomaiLabel(String luomaiLabel) {
            this.luomaiLabel = luomaiLabel;
        }

        public String getToothtrace() {
            return toothtrace;
        }

        public void setToothtrace(String toothtrace) {
            this.toothtrace = toothtrace;
        }

        public String getYuban() {
            return yuban;
        }

        public void setYuban(String yuban) {
            this.yuban = yuban;
        }

        public String getSplitAreaLabel() {
            return splitAreaLabel;
        }

        public void setSplitAreaLabel(String splitAreaLabel) {
            this.splitAreaLabel = splitAreaLabel;
        }

        public String getHsvH() {
            return hsvH;
        }

        public void setHsvH(String hsvH) {
            this.hsvH = hsvH;
        }

        public String getHsvS() {
            return hsvS;
        }

        public void setHsvS(String hsvS) {
            this.hsvS = hsvS;
        }

        public String getHsvV() {
            return hsvV;
        }

        public void setHsvV(String hsvV) {
            this.hsvV = hsvV;
        }

        public String getRectContour() {
            return rectContour;
        }

        public void setRectContour(String rectContour) {
            this.rectContour = rectContour;
        }

        public String getGanyuXian() {
            return ganyuXian;
        }

        public void setGanyuXian(String ganyuXian) {
            this.ganyuXian = ganyuXian;
        }
    }

    /**
     * 计算图
     */
    public static class CaluateImage {
        /**
         * 舌面分割图存放路径
         **/
        private String splitFilePath;

        /**
         * 近期图存放路径
         **/
        private String renctFilePath;

        /**
         * json存放路径
         **/
        @JsonIgnore
        private String jsonFilePath;

        /**
         * 舌下分割图路径
         */
        private String splitBackFilePath;

        public String getSplitFilePath() {
            return splitFilePath;
        }

        public void setSplitFilePath(String splitFilePath) {
            this.splitFilePath = splitFilePath;
        }

        public String getRenctFilePath() {
            return renctFilePath;
        }

        public void setRenctFilePath(String renctFilePath) {
            this.renctFilePath = renctFilePath;
        }

        public String getJsonFilePath() {
            return jsonFilePath;
        }

        public void setJsonFilePath(String jsonFilePath) {
            this.jsonFilePath = jsonFilePath;
        }

        public String getSplitBackFilePath() {
            return splitBackFilePath;
        }

        public void setSplitBackFilePath(String splitBackFilePath) {
            this.splitBackFilePath = splitBackFilePath;
        }
    }
}
