package com.mxjsxz.demo.model.vo.composite;

import java.util.List;

/**
 * 证素DTO
 *
 * @author xuwenbing
 * @date 2022-7-8
 */
public class SyndromesElementVO {
    /**
     * 病性证素
     */
    private List<Element> natureList;

    /**
     * 病位证素
     */
    private List<Element> locationList;

    public static class Element {
        /**
         * 证素编号
         */
        private String code;
        /**
         * 证素名称
         */
        private String name;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public List<Element> getNatureList() {
        return natureList;
    }

    public void setNatureList(List<Element> natureList) {
        this.natureList = natureList;
    }

    public List<Element> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<Element> locationList) {
        this.locationList = locationList;
    }
}
