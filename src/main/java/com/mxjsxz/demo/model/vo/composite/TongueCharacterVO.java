package com.mxjsxz.demo.model.vo.composite;

/**
 * 舌象特征
 *
 * @author xuwenbing
 * @date 2022-9-30
 */
public class TongueCharacterVO {
    /**
     * 特征编码
     */
    private String code;

    /**
     * 曲线排序
     */
    private Integer curveSort;

    /**
     * 特征名称
     */
    private String name;

    /**
     * 特征描述
     */
    private String description;

    /**
     * 2020-01-13 增加 简述 字段
     */
    private String simpleDescription;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCurveSort() {
        return curveSort;
    }

    public void setCurveSort(Integer curveSort) {
        this.curveSort = curveSort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSimpleDescription() {
        return simpleDescription;
    }

    public void setSimpleDescription(String simpleDescription) {
        this.simpleDescription = simpleDescription;
    }
}
