package com.mxjsxz.demo.model.vo.composite;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

/**
 * 24节气患病率DTO
 *
 * @author zp
 * @date 2022-09-27
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IllProbabilityVO implements Serializable {

    private static final long serialVersionUID = 6088785722474226468L;

    /**
     * 患病评分
     */
    @Deprecated
    private Double scope;

    /**
     * 提示语
     */
    @Deprecated
    private String tip;

    /**
     * 说明
     */
    private String explanation;

    /**
     * 节气患病率配置
     */
    private List<Element> illProbabilities;


    public static class Element implements Serializable {

        private static final long serialVersionUID = -8482554283025953873L;

        /**
         * 节气名称
         */
        private String name;

        /**
         * 节气患病率
         */
        private Integer probability;

        /**
         * 养生提示提示
         */
        private String tip;

        /**
         * 节气患病概率等级
         */
        private String level;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getProbability() {
            return probability;
        }

        public void setProbability(Integer probability) {
            this.probability = probability;
        }

        public String getTip() {
            return tip;
        }

        public void setTip(String tip) {
            this.tip = tip;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }
    }

    public Double getScope() {
        return scope;
    }

    public void setScope(Double scope) {
        this.scope = scope;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public List<Element> getIllProbabilities() {
        return illProbabilities;
    }

    public void setIllProbabilities(List<Element> illProbabilities) {
        this.illProbabilities = illProbabilities;
    }
}
