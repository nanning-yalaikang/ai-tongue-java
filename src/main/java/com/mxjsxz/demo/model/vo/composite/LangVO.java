package com.mxjsxz.demo.model.vo.composite;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Map;

/**
 * 国际化资源
 *
 * @author xdl
 * @date 2023-06-28
 */
public class LangVO implements Serializable {

    private static final long serialVersionUID = -6635703615089832469L;

    /**
     * 繁体中文
     */
    @JSONField(name = "zh-Hant")
    private Map<String, String> zhHant;

    /**
     * 英语
     */
    @JSONField(name = "en")
    private Map<String, String> en;

    public Map<String, String> getZhHant() {
        return zhHant;
    }

    public void setZhHant(Map<String, String> zhHant) {
        this.zhHant = zhHant;
    }

    public Map<String, String> getEn() {
        return en;
    }

    public void setEn(Map<String, String> en) {
        this.en = en;
    }
}
