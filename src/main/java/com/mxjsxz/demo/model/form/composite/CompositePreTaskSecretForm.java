package com.mxjsxz.demo.model.form.composite;

/**
 * 预检测任务form
 *
 * @author xuwenbing
 * @date 2023-10-10
 */
public class CompositePreTaskSecretForm {
    /**
     * <pre>
     *  舌象和面象图片类型
     *  1文件对象
     *  2网络地址
     * </pre>
     */
    private Short imageType;

    /**
     * 结果回调地址
     */
    private String returnUrl;

    ////////////// 面诊 //////////////
    /**
     * 正面部图地址，支持http/https;imageType=2时必传
     */
    private String faceImgUrl;

    /**
     * 左侧面部图地址，支持http/https;imageType=2时有效
     */
    private String faceLeftImgUrl;

    /**
     * 右侧面部图地址，支持http/https;imageType=2时有效
     */
    private String faceRightImgUrl;

    ////////////// 舌诊 //////////////
    /**
     * 正面部图地址，支持http/https;imageType=2时必传
     */
    private String tongueImgUrl;

    /**
     * 左侧面部图地址，支持http/https;imageType=2时有效
     */
    private String tongueBackImgUrl;

    public Short getImageType() {
        return imageType;
    }

    public void setImageType(Short imageType) {
        this.imageType = imageType;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getFaceImgUrl() {
        return faceImgUrl;
    }

    public void setFaceImgUrl(String faceImgUrl) {
        this.faceImgUrl = faceImgUrl;
    }

    public String getFaceLeftImgUrl() {
        return faceLeftImgUrl;
    }

    public void setFaceLeftImgUrl(String faceLeftImgUrl) {
        this.faceLeftImgUrl = faceLeftImgUrl;
    }

    public String getFaceRightImgUrl() {
        return faceRightImgUrl;
    }

    public void setFaceRightImgUrl(String faceRightImgUrl) {
        this.faceRightImgUrl = faceRightImgUrl;
    }

    public String getTongueImgUrl() {
        return tongueImgUrl;
    }

    public void setTongueImgUrl(String tongueImgUrl) {
        this.tongueImgUrl = tongueImgUrl;
    }

    public String getTongueBackImgUrl() {
        return tongueBackImgUrl;
    }

    public void setTongueBackImgUrl(String tongueBackImgUrl) {
        this.tongueBackImgUrl = tongueBackImgUrl;
    }
}
