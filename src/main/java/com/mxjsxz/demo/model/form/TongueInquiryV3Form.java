package com.mxjsxz.demo.model.form;

import java.util.List;

/**
 * 问诊回答参数(中医四诊)
 *
 * @author xdl
 * @date 2022-6-1
 */
public class TongueInquiryV3Form {

    /**
     * 第三方单据ID
     */
    private String outId;

    /**
     * 回答的症状
     */
    private List<Answer> answer;

    /**
     * 回答的症状
     */
    public static class Answer {

        /**
         * 症状类型编码
         */
        private String code;

        /**
         * 症状选项名称串
         * 逗号分隔
         */
        private String options;

        public Answer(String code, String options) {
            this.code = code;
            this.options = options;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getOptions() {
            return options;
        }

        public void setOptions(String options) {
            this.options = options;
        }
    }

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public List<Answer> getAnswer() {
        return answer;
    }

    public void setAnswer(List<Answer> answer) {
        this.answer = answer;
    }
}
