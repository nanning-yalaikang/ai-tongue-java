package com.mxjsxz.demo.model.form;

/**
 * 中医四诊 form
 * @author xuwenbing
 * @date 2023-10-17
 */
public class TongueTaskV3Form {
    /**
     * 第三方单据Id
     */
    private String outId;

    /**
     * 年龄:大于等于0
     */
    private Integer age;

    /**
     * 性别：0未知 1男 2女
     */
    private Short sex;

    /**
     * 疾病编码
     */
    private String diseaseCode;

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public String getDiseaseCode() {
        return diseaseCode;
    }

    public void setDiseaseCode(String diseaseCode) {
        this.diseaseCode = diseaseCode;
    }
}
