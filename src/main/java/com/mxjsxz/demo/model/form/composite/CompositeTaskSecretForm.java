package com.mxjsxz.demo.model.form.composite;

import java.math.BigDecimal;

/**
 * 提交检测form
 *
 * @author xuwenbing
 * @date 2023-10-10
 */
public class CompositeTaskSecretForm {
    /**
     * 年龄
     */
    private Integer age;

    /**
     * 性别
     * 0未知
     * 1男
     * 2女
     */
    private Short sex;

    /**
     * 身高，单位cm
     */
    private BigDecimal height;

    /**
     * 体重，单位kg
     */
    private BigDecimal weight;

    /**
     * <strong>疾病编码</strong>
     * <p>1. 非必填，未填写时默认为“体质健康（C00.D00）”</p>
     * <p>2. 目前支持：C00、C01、C02和C04类别下的疾病编码</p>
     */
    private String diseaseCode;

    /**
     * 返回结果语言
     * <p>
     * traditional/simple, default simple
     */
    private String language;

    /**
     * 是否需要问诊流程
     */
    private Boolean requireInquiry;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public String getDiseaseCode() {
        return diseaseCode;
    }

    public void setDiseaseCode(String diseaseCode) {
        this.diseaseCode = diseaseCode;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Boolean getRequireInquiry() {
        return requireInquiry;
    }

    public void setRequireInquiry(Boolean requireInquiry) {
        this.requireInquiry = requireInquiry;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }
}
