package com.mxjsxz.demo.service.impl;

import com.mxjsxz.demo.service.IRestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

/**
 * 发送请求工具
 *
 * @author xuwenbing
 * @date 2019-05-05
 */
@Service
public class RestTemplateServiceImpl implements IRestTemplateService {

    private final RestTemplate restTemplate;

    @Autowired
    public RestTemplateServiceImpl(@Qualifier("skipSslRestTemplate") RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public <T> T postForObject(String url, MultiValueMap<String, Object> params, Class<T> responseType) {
        return restTemplate.postForObject(url, params, responseType);
    }

    @Override
    public <T> T postForObject(String url, Map<String, String> headers
            , MultiValueMap<String, Object> params, Class<T> responseType) {
        HttpHeaders httpHeaders = new HttpHeaders();
        for (Map.Entry<String, String> header : headers.entrySet()) {
            httpHeaders.add(header.getKey(), header.getValue());
        }
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(params, httpHeaders);
        return restTemplate.postForObject(url, httpEntity, responseType);
    }

    @Override
    public <T> T postJson(String returnUrl, Map<String, String> headers, Map<String, Object> body,
                          Class<T> responseType) {
        Iterator<Map.Entry<String, String>> var6 = headers.entrySet().iterator();

        HttpHeaders httpHeaders = new HttpHeaders();
        while (var6.hasNext()) {
            Map.Entry<String, String> header = var6.next();
            httpHeaders.add(header.getKey(), header.getValue());
        }
        // 设置请求的 Content-Type 为 application/json
        httpHeaders.put(HttpHeaders.CONTENT_TYPE, Collections.singletonList(MediaType.APPLICATION_JSON_VALUE));
        // 设置 Accept 向服务器表明客户端可处理的内容类型
        httpHeaders.put(HttpHeaders.ACCEPT, Collections.singletonList(MediaType.APPLICATION_JSON_VALUE));

        // 直接将实体 Product 作为请求参数传入，底层利用 Jackson 框架序列化成 JSON 串发送请求
        HttpEntity<Map<String, Object>> request = new HttpEntity<>(body, httpHeaders);
        ResponseEntity<T> exchangeResult = restTemplate
                .exchange(returnUrl, HttpMethod.POST, request, responseType);
        if (HttpStatus.OK.equals(exchangeResult.getStatusCode())) {
            return exchangeResult.getBody();
        }
        return null;
    }


    @Override
    public FileSystemResource getFileSystemResource(File file) {
        return new FileSystemResource(file);
    }

    @Override
    public FileSystemResource getClassPathFileSystemResource(String path) {
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource(path);
        File file = new File(resource.getFile());
        return this.getFileSystemResource(file);
    }

}
