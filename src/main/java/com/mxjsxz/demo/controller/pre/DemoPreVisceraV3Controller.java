package com.mxjsxz.demo.controller.pre;

import com.mxjsxz.demo.constants.AiTongueConstant;
import com.mxjsxz.demo.model.form.TongueInquiryV3Form;
import com.mxjsxz.demo.model.form.TongueTaskV3Form;
import com.mxjsxz.demo.model.vo.ResultVO;
import com.mxjsxz.demo.model.vo.ReturnVO;
import com.mxjsxz.demo.model.vo.V3TongueReturnVO;
import com.mxjsxz.demo.properties.AiTongueProperties;
import com.mxjsxz.demo.service.IRestTemplateService;
import com.mxjsxz.demo.utils.AesSimpleUtil;
import com.mxjsxz.demo.utils.JsonUtil;
import com.mxjsxz.demo.utils.RsaSimpleUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.mxjsxz.demo.constants.AiTongueConstant.SUCCESS;

/**
 * 中医四诊 API
 *
 * @author xuwenbing
 * @date 2019-06-10
 */
@RestController
@RequestMapping("viscera/v3/pre")
public class DemoPreVisceraV3Controller {
    private final Logger log = LoggerFactory.getLogger(DemoPreVisceraV3Controller.class);
    private final AiTongueProperties aiTongueProperties;
    private final IRestTemplateService restTemplateService;
    private Integer outId = 1;

    @Autowired
    public DemoPreVisceraV3Controller(AiTongueProperties aiTongueProperties, IRestTemplateService restTemplateService) {
        this.aiTongueProperties = aiTongueProperties;
        this.restTemplateService = restTemplateService;
    }

    /**
     * 阶段一：验证舌象
     *
     * @return
     */
    @PostMapping("index")
    public String index() throws Exception {
        // 1.获取accesstoken
        String accesstoken = this.getAccessToken(aiTongueProperties.getDevId(), aiTongueProperties.getDevSecret());
        if (StringUtils.isBlank(accesstoken)) {
            return AiTongueConstant.ERROR;
        }
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + accesstoken);

        // 2.准备待检测的数据：第三方单据id、舌面图片、舌下图片、年龄、性别和结果接收地址
        File imageFile = new File(this.getClass().getClassLoader().getResource("static/tongue2.jpg").getFile());
        File backImageFile = new File(this.getClass().getClassLoader().getResource("static/tongueBack2.jpg").getFile());
        //您的结果接收地址
        String returnUrl = "https://your.domain/viscera/v3/pre/resultReturn";

        // 3.签名数据
        String signature = RsaSimpleUtil.sign(Integer.toString(outId), aiTongueProperties.getDevRsaPrivateKey());

        // 4.上传检测
        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        params.add("outId", outId);
        params.add("returnUrl", returnUrl);
        params.add("signature", signature);
        params.add("images", restTemplateService.getFileSystemResource(imageFile));
        params.add("backImages", restTemplateService.getFileSystemResource(backImageFile));
        // 开启下面的代码表示使用网络舌象图片,支持http/https
//        params.add("imageType", 2);
//        params.add("imageUrl", "https://your.domaim/tongue.jpg");
//        params.add("backImageUrl", "https://your.domaim/tongueBack.jpg");

        // 当resultVO.code=0表示上传成功：
        // （1）检测的结果会回传到您设置的returnUrl上
        ResultVO resultVO = restTemplateService.postForObject(aiTongueProperties.getPreTongueUrl(), headers, params, ResultVO.class);
        log.info("index:{}", JsonUtil.toJson(resultVO));
        return AiTongueConstant.OK;
    }

    /**
     * 阶段二：1、确认提交检测
     *
     * @return
     */
    @PostMapping("task")
    public String task() throws Exception {
        // 1.获取accesstoken
        String accesstoken = this.getAccessToken(aiTongueProperties.getDevId(), aiTongueProperties.getDevSecret());
        if (StringUtils.isBlank(accesstoken)) {
            return AiTongueConstant.ERROR;
        }
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + accesstoken);

        // 2.准备待检测的数据：第三方单据id、舌面图片、舌下图片、年龄、性别和结果接收地址
        TongueTaskV3Form form = new TongueTaskV3Form();
        form.setOutId(Integer.toString(outId));
        form.setAge(4);
        form.setSex((short) 1);
        // 目前仅支持检测类别C08下的编码，详见：https://www.ai-tongue.com/doc/global/enum.html#儿童常见疾病（C08）
        form.setDiseaseCode("C08.D00");

        // 3.加密和签名数据
        String signature = RsaSimpleUtil.sign(form.getOutId(), aiTongueProperties.getDevRsaPrivateKey());
        String sourceData = JsonUtil.toJson(form);
        String encryptData = AesSimpleUtil.encrypt(sourceData, aiTongueProperties.getAesKey());

        // 4.上传检测
        Map<String, Object> params = new HashMap<>();
        params.put("signature", signature);
        params.put("encryptData", encryptData);
        // 当resultVO.code=0表示上传成功：
        // （1）resultVO.data表示预计等待检测时间，
        // （2）检测的结果会回传到您设置的returnUrl上
        ResultVO resultVO = restTemplateService.postJson(aiTongueProperties.getTongueTaskV3Url(),
                headers, params, ResultVO.class);
        log.info("task:{}", JsonUtil.toJson(resultVO));
        return AiTongueConstant.OK;
    }

    /**
     * 阶段二：2、回答问诊问题
     *
     * @return
     */
    @PostMapping("inquiry")
    public String inquiry() throws Exception {
        // 1.获取accesstoken
        String accesstoken = this.getAccessToken(aiTongueProperties.getDevId(), aiTongueProperties.getDevSecret());
        if (StringUtils.isBlank(accesstoken)) {
            return AiTongueConstant.ERROR;
        }
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + accesstoken);

        // 2.准备问诊回答数据：第三方单据id和问诊回答数据
        TongueInquiryV3Form.Answer[] answers = {
                new TongueInquiryV3Form.Answer("IS0101", "唇红,咽喉红肿"),
                new TongueInquiryV3Form.Answer("IS0081", "腹胀"),
                new TongueInquiryV3Form.Answer("IS0102", "口渴,消化系统紊乱"),
                new TongueInquiryV3Form.Answer("IS0082", "手足心热"),
                new TongueInquiryV3Form.Answer("IS0083", "善悲易哭")
        };
        TongueInquiryV3Form form = new TongueInquiryV3Form();
        form.setOutId(Integer.toString(outId));
        form.setAnswer(Arrays.asList(answers));

        // 3.加密和签名数据
        String signature = RsaSimpleUtil.sign(form.getOutId(), aiTongueProperties.getDevRsaPrivateKey());
        String sourceData = JsonUtil.toJson(form);
        String encryptData = AesSimpleUtil.encrypt(sourceData, aiTongueProperties.getAesKey());

        // 4.上传问诊答案
        Map<String, Object> params = new HashMap<>();
        params.put("signature", signature);
        params.put("encryptData", encryptData);
        // 当resultVO.code=0表示上传成功：
        // （1）resultVO.data表示预计等待检测时间，
        // （2）检测的结果会回传到您设置的returnUrl上
        ResultVO resultVO = restTemplateService
                .postJson(aiTongueProperties.getTongueInquiryV3TaskUrl(), headers, params, ResultVO.class);
        log.info("inquiry:{}", JsonUtil.toJson(resultVO));
        return AiTongueConstant.OK;
    }


    /**
     * 2、结果接收地址
     *
     * @param returnVO
     * @return
     */
    @PostMapping("resultReturn")
    public String resultReturn(ReturnVO returnVO) throws Exception {
        log.info("viscera v3 resultReturn：{}", JsonUtil.toJson(returnVO));
        // 1. 验证签名
        boolean verify = RsaSimpleUtil.verify(returnVO.getOutId(), returnVO.getSignature(), aiTongueProperties.getRsaPublicKey());
        if (verify) {
            // 2. 只有验签通过后才进行业务操作
            // 3. 解密数据
            String decryptData = AesSimpleUtil.decrypt(returnVO.getEncryptData(), aiTongueProperties.getAesKey());
            // 4. decryptData可以转化为TongueReturnVO对象,具体每个字段含义见TongueReturnVO
            // 其中returnType表示返回结果的类型
            // returnType=0 说明返回的问诊题目，此时您可以保存结果并返回给用户，用户回答问诊后调用“阶段二：2、回答问诊问题”接口
            // returnType=1 说明返回的最终体质结果，此时您可以保存结果并返回给用户
            // returnType=2 说明返回的失败提示信息，此时您可以反馈给用户
            // returnType=31 预判为合法图片，此时您可以调用“阶段二：确认提交检测”接口
            // returnType=32 预判为不合法图片，此时您可以反馈给用户，并且不推荐继续调用“阶段二：1、确认提交检测”接口
            log.info("viscera v3 decryptData：{}", decryptData);
            V3TongueReturnVO v3TongueReturnVO = JsonUtil.parseObject(decryptData, V3TongueReturnVO.class);
            return SUCCESS;
        }
        log.info("验证签名失败");
        return AiTongueConstant.ERROR;
    }

    /**
     * 获取access_token（获取后建议缓存，一般30分钟有效）
     *
     * @param devId
     * @param devSecret
     * @return
     */
    private String getAccessToken(String devId, String devSecret) {
        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        params.add("devid", devId);
        params.add("devsecret", devSecret);
        ResultVO accessTokenResult = restTemplateService.postForObject(aiTongueProperties.getGetTokenUrl(), params, ResultVO.class);
        log.info("access_token result:{}", JsonUtil.toJson(accessTokenResult));
        if (accessTokenResult.getCode() != 0) {
            return null;
        }
        return ((Map) accessTokenResult.getData()).get("access_token").toString();
    }
}
